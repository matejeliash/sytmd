#!/usr/bin/env python3


from multiprocessing import cpu_count
from pytube import YouTube
from pytube import Playlist
import os
from urllib import request
from mutagen.mp4 import MP4, MP4Cover
import tqdm
from tqdm.contrib.concurrent import process_map 
import sys
from ytmusicapi import YTMusic
from mutagen.flac import FLAC, Picture
from mutagen.oggopus import OggOpus
import base64

def get_song_name_from_archive(video_id):
    
    with open("archive.txt","r") as archive:
        archive_lines = archive.readlines()

    for i in archive_lines:
        if video_id in i:
            return i[12:].strip()
    
    return None




def is_in_archive(video_id):

    with open("archive.txt","r") as archive:
        archive_text = archive.read()
        if video_id  in archive_text:
            return True

    return False


def create_m3u(url):

    playlist_obj=Playlist(url)
    playlist_name=playlist_obj.title + ".m3u"
    if not os.path.isfile(playlist_name ):
        with open(playlist_name , "w") as f:
            yt_objs = playlist_obj.videos
            for i in yt_objs:
                filepath=get_song_name_from_archive(i.video_id)
                f.write(filepath + "\n")
    print("Created playlist " + playlist_name)







def write_to_archive(video_id, filename):
    
    if not is_in_archive(video_id):
        with open("archive.txt","a") as archive2:
            archive2.write(f"{video_id} {filename}\n")



def get_metadata(videoId):

    try:    
        ytmusic = YTMusic()
        song = ytmusic.get_watch_playlist(videoId=videoId)

        song_info=song['tracks'][0]

        image_url=song_info['thumbnail'][-1]['url']

        response = request.urlretrieve( image_url, videoId + ".jpg")

        artists_list = [ x['name'] for x in  song_info['artists']]
        album_artist=artists_list[0]
        artists_string = ", ".join(artists_list)
        album = song_info['album']['name']
        title=song_info['title']
        
        return [artists_string, title, album, videoId, album_artist]
    
    #except:
    #    print("Error: get_metadata -> " + videoId)
    #    return None
    except Exception as e:
        print(e)
        return None






   

def write_metadata(metadata):

        image_name = f"{metadata[3]}.jpg"
        song_name = f"{metadata[0]} - {metadata[1]}.m4a"


        audio = MP4(song_name)

        audio["\xa9ART"] = metadata[0]
        audio["\xa9nam"] = metadata[1]
        audio["\xa9alb"] = metadata[2]
        audio["\xa9cmt"] = metadata[3]
        audio["aART"] = metadata[4]


        if os.path.exists(image_name):
            with open(image_name, "rb") as f:
                audio["covr"] = [
                    MP4Cover(f.read(), imageformat=MP4Cover.FORMAT_JPEG)
    ]

        audio.save()

        if os.path.exists(image_name):
            os.remove(image_name)



def write_metadata_opus(metadata):

        image_name = f"{metadata[3]}.jpg"
        song_name = f"{metadata[0]} - {metadata[1]}.opus"


        audio = OggOpus(song_name)

        audio["TITLE"] = metadata[1]
        audio["ARTIST"] = metadata[0]
        audio["ALBUMARTIST"] = metadata[4]
        audio["ALBUM"] = metadata[2]
        audio["COMMENT"] = metadata[3]
        pic = Picture()
        with open(image_name, 'rb') as thumbfile:
            pic.data = thumbfile.read()
        pic.type = 3  # front cover
        pic.mime = "image/jpeg"
        audio['metadata_block_picture'] = base64.b64encode(pic.write()).decode('ascii')
        audio.save()

        if os.path.exists(image_name):
            os.remove(image_name)
        


def download_song(video_id):

    try:

        metadata=get_metadata(video_id)
        
        if metadata != None:
            song_name = f"{metadata[0]} - {metadata[1]}.m4a"
            print("Downloading: " + song_name)
            os.system("yt-dlp -f 140 --output  '%(id)s.%(ext)s'  -- " + video_id   + " > /dev/null 2> /dev/null")
            os.rename(video_id + ".m4a",song_name )
            write_metadata(metadata)
        
            write_to_archive(video_id,song_name)

        else:
            print("Downloading: " + video_id)
            os.system("yt-dlp -f 140   -- " + video_id    + " > /dev/null 2> /dev/null")

    
    except:
        print("Could not find song with this URL !!! -> " + video_id)



def download_song_opus(video_id):

    try:

        metadata=get_metadata(video_id)
        
        if metadata != None:
            song_name = f"{metadata[0]} - {metadata[1]}.opus"
            print("Downloading: " + song_name)
            dv=os.system("yt-dlp -f 251 -x --output  '%(id)s.%(ext)s'  -- " + video_id   + " > /dev/null 2> /dev/null")
            if dv == 0:
                os.rename(video_id + ".opus",song_name )
                write_metadata_opus(metadata)
            else:
                os.system("yt-dlp -f 140 --output  '%(id)s.%(ext)s'  -- " + video_id   + " > /dev/null 2> /dev/null")
                os.rename(video_id + ".m4a",song_name )
                write_metadata(metadata)

        
            write_to_archive(video_id,song_name)

        else:
            print("Downloading: " + video_id)
            os.system("yt-dlp -f 140   -- " + video_id    + " > /dev/null 2> /dev/null")

    
    except:
        print("Could not find song with this URL !!! -> " + video_id)





def parse_and_start():

    playlists = []
    songs = []
    prev = -1
    format="opus"
    args= sys.argv[1:]
    print(args)
    
    for i  in range(len(args)) :
        print( args[i])
        if  "list=PLV"   in args[i]:
            try:
                playlist_obj=Playlist(args[i])
                playlists.append(playlist_obj.playlist_url)
                songs.extend(playlist_obj.video_urls)
            except:
                print("Skipping  " + args[i])

        elif  "list=OLAK"  in args[i]:
            try:
                playlist_obj=Playlist(args[i])
                songs.extend(playlist_obj.video_urls)
            except:
                print("Skipping  " + args[i])
        elif "watch?v" in args[i]:
            try:
                yt_obj=YouTube(args[i] )
                songs.append(yt_obj.video_id)
            except:
                print("Skipping  " + args[i])
        elif   args[i] == '-f'  :
            prev=i

        elif   args[i] == 'm4a'  :
            if i-1 == prev:
                format="m4a"
            else:
                print("Error parsing format")
                exit()

        elif   args[i] == 'opus'  :
            if i-1 == prev:
                format="opus"
            else:
                print("Error parsing format")
                exit()

        else:
            print("Exiting error in parsing !!!")
            #exit()



    cleaned_songs = list( dict.fromkeys(songs) )
    final_songs = [song.replace('https://www.youtube.com/watch?v=','') for song in cleaned_songs]



    final_playlists = list( dict.fromkeys(playlists) )

    final_songs= [song for song in final_songs if not is_in_archive(song)] 


    if len(final_songs) > 0:
        if format == 'opus':
            r = process_map(download_song_opus,final_songs, max_workers=cpu_count())
        else:
            r = process_map(download_song,final_songs, max_workers=cpu_count())

    else:
        print("Nothing to download !!!")

    for i in playlists:
        create_m3u(i)






if __name__ == '__main__':


    parse_and_start()






'''


def download_playlist_multi(playlist_url):

    try:
        playlist_obj=Playlist(playlist_url)

        song_urls=playlist_obj.video_urls
        playlist_name = playlist_obj.title

        if not os.path.exists(playlist_name):
            os.mkdir(playlist_name)
            os.chdir(playlist_name)
            r = process_map(download_song,song_urls, max_workers=cpu_count())
            rename_album(playlist_name)
        else :
            print("Can't create folder for playlist / album  !!!")

    except:
        print("Could not find playlist/album with this URL !!!")




'''






















