# sytmd ( Simple YouTube Music Downloader )

Simple python script that let's you get music from YTMusic. 

## Features

- retrieves songs, albums and playlists from YTMusic
- pongs are downloaded in m4a format
- program adds artist,album and title tag to file with mutagen
- uses multiprocessing ( downloads more songs at the same time )
- albums/playlists are places in separate directories

This program can only correctly get metadata from YTMusic songs ( videos on Topic channels ). 



## Usage
To get the urls from YTMusic click on three dots then share button and copy url.
 
./sytmd [urls] 

## Dependencies 

tqdm pytube mutagen ytmusicapi yt-dlp



## Licence

This project is Licensed under the MIT License.
