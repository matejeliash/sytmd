#!/usr/bin/env python3


from multiprocessing import cpu_count
from pytube import YouTube
from pytube import Playlist
import os
from urllib import request
from mutagen.mp4 import MP4, MP4Cover
import tqdm
from tqdm.contrib.concurrent import process_map 
import sys
from ytmusicapi import YTMusic



def get_metadata(videoId):

    try:    
        ytmusic = YTMusic()
        song = ytmusic.get_watch_playlist(videoId=videoId)

        song_info=song['tracks'][0]

        image_url=song_info['thumbnail'][-1]['url']

        response = request.urlretrieve( image_url, videoId + ".jpg")

        artists_list = [ x['name'] for x in  song_info['artists']]
        album_artist=artists_list[0]
        artists_string = ", ".join(artists_list)
        album = song_info['album']['name']
        title=song_info['title']
        
        return [artists_string, title, album, videoId, album_artist]
    
    except:
        print("Error: get_metadata -> " + videoId)
        return None





   

def write_metadata(yt_obj, metadata):

        image_name = f"{yt_obj.video_id}.jpg"
        song_name = f"{metadata[0]} - {metadata[1]}.m4a"


        audio = MP4(song_name)

        audio["\xa9ART"] = metadata[0]
        audio["\xa9nam"] = metadata[1]
        audio["\xa9alb"] = metadata[2]
        audio["\xa9cmt"] = metadata[3]
        audio["aART"] = metadata[4]


        if os.path.exists(image_name):
            with open(image_name, "rb") as f:
                audio["covr"] = [
                    MP4Cover(f.read(), imageformat=MP4Cover.FORMAT_JPEG)
    ]

        audio.save()

        if os.path.exists(image_name):
            os.remove(image_name)





def download_song(url):

    try:

        yt_obj=YouTube(url)
        metadata=get_metadata(yt_obj.video_id)
        
        if metadata != None:
            song_name = f"{metadata[0]} - {metadata[1]}.m4a"
            print("Downloading: " + song_name)
            os.system("yt-dlp -f 140 --output  '%(id)s.%(ext)s'  -- " + yt_obj.video_id    + " > /dev/null 2> /dev/null")
            os.rename(yt_obj.video_id + ".m4a",song_name )
            write_metadata(yt_obj,metadata)
        else:
            print("Downloading: " + yt_obj.title)
            os.system("yt-dlp -f 140   -- " + yt_obj.video_id    + " > /dev/null 2> /dev/null")

    
    except:
        print("Could not find song with this URL !!! -> " + url)



def rename_album(playlist_name):

    try:
    
        final_name = None
        if  playlist_name.startswith("Album"):
            song=os.listdir(".")[0]
            artist=song.split(sep=" - ")[0]
            if ', ' in artist:
                artist=artist.split(sep=", ")[0]
            final_name=playlist_name.replace("Album",artist)
            os.chdir("..")
            os.rename(playlist_name,final_name)
        else:
            os.chdir("..")
    
    except:
        print("Error: rename_album()")



        


def download_playlist_multi(playlist_url):

    try:
        playlist_obj=Playlist(playlist_url)

        song_urls=playlist_obj.video_urls
        playlist_name = playlist_obj.title

        if not os.path.exists(playlist_name):
            os.mkdir(playlist_name)
            os.chdir(playlist_name)
            r = process_map(download_song,song_urls, max_workers=cpu_count())
            rename_album(playlist_name)
        else :
            print("Can't create folder for playlist / album  !!!")

    except:
        print("Could not find playlist/album with this URL !!!")



def parse_and_start():

    playlists = []
    songs = []


    
    for arg in sys.argv[1:] :
            if  "list=PLV"   in arg:
                playlists.append(arg)
            elif  "list=OLAK"  in arg:
                playlists.append(arg)
            elif "watch?v" in arg:
                songs.append(arg)
            else:
                print("Wrong arguments !!! ")

    
    for playlist in playlists:
        download_playlist_multi(playlist)

    if songs is not None:
        r = process_map(download_song,songs, max_workers=cpu_count())






if __name__ == '__main__':


    parse_and_start()




